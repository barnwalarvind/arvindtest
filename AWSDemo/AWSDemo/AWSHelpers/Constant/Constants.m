//
// Copyright 2014-2016 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

#import "Constants.h"

AWSRegionType const CognitoIdentityUserPoolRegion = AWSRegionUSEast1;
NSString *const CognitoIdentityUserPoolId = @"us-east-1_c0NVwOIXe";
NSString *const CognitoIdentityUserPoolAppClientId = @"7s17rampsl9qha6i0ov0f0po98";
NSString *const CognitoIdentityUserPoolAppClientSecret = @"1fkkqo9s1os46qg7qroi564fepkuv5070g5h1310c36v7rgvemlq";
