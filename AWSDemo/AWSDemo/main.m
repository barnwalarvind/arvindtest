//
//  main.m
//  AWSDemo
//
//  Created by Infoicon on 08/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
