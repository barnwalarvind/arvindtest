//
//  AppDelegate.h
//  AWSDemo
//
//  Created by Infoicon on 08/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSCognitoIdentityProvider.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,AWSCognitoIdentityInteractiveAuthenticationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong) UINavigationController *navigationController;

@end

