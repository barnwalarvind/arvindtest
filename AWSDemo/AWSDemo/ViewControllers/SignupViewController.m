//
//  ViewController.m
//  AWSDemo
//
//  Created by Infoicon on 08/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import "SignupViewController.h"
#import "AWSCognitoIdentityProvider.h"


@interface SignupViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) AWSCognitoIdentityUserPool * pool;
@property (nonatomic, strong) NSString* sentTo;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pool = [AWSCognitoIdentityUserPool CognitoIdentityUserPoolForKey:@"UserPool"];

   
}

- (IBAction)btnSignup:(id)sender {
    
    NSMutableArray * attributes = [NSMutableArray new];
    AWSCognitoIdentityUserAttributeType * phone = [AWSCognitoIdentityUserAttributeType new];
    phone.name = @"phone_number";
    phone.value = self.txtPhone.text;
    AWSCognitoIdentityUserAttributeType * email = [AWSCognitoIdentityUserAttributeType new];
    email.name = @"email";
    email.value = self.txtEmail.text;
    
    if(![@"" isEqualToString:phone.value]){
        [attributes addObject:phone];
    }
    if(![@"" isEqualToString:email.value]){
        [attributes addObject:email];
    }
    
    //sign up the user
    [[self.pool signUp:self.txtUsername.text password:self.txtPassword.text userAttributes:attributes validationData:nil] continueWithBlock:^id _Nullable(AWSTask<AWSCognitoIdentityUserPoolSignUpResponse *> * _Nonnull task) {
        NSLog(@"Successful signUp user: %@",task.result.user.username);
        dispatch_async(dispatch_get_main_queue(), ^{
            if(task.error){
                [[[UIAlertView alloc] initWithTitle:task.error.userInfo[@"__type"]
                                            message:task.error.userInfo[@"message"]
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil] show];
            }else if(task.result.user.confirmedStatus != AWSCognitoIdentityUserStatusConfirmed){
                self.sentTo = task.result.codeDeliveryDetails.destination;
                [self performSegueWithIdentifier:@"SignupToVerification" sender:sender];
            }
            else{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }});
        return nil;
    }];

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([@"SignupToVerification" isEqualToString:segue.identifier]){
        VerificationViewController *cvc = segue.destinationViewController;
        cvc.sentTo = self.sentTo;
        cvc.user = [self.pool getUser:self.txtUsername.text];
    }
}


/**
 Ensure phone number starts with country code i.e. (+1)
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if(textField == self.txtPhone){
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^\\+(|\\d)*$" options:0 error:nil];
        NSString *proposedPhone = [self.txtPhone.text stringByReplacingCharactersInRange:range withString:string];
        if(proposedPhone.length != 0){
            return [regex numberOfMatchesInString:proposedPhone options:NSMatchingAnchored range:NSMakeRange(0, proposedPhone.length)]== 1;
        }
    }
    return YES;
}


@end
