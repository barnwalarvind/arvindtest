//
//  ViewController.h
//  AWSDemo
//
//  Created by Infoicon on 08/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerificationViewController.h"

@interface SignupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;

@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UITextField *txtPhone;

@end

