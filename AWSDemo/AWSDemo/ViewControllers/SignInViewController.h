//
//  SignInViewController.h
//  AWSDemo
//
//  Created by Infoicon on 09/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSCognitoIdentityProvider.h"

@interface SignInViewController : UIViewController<AWSCognitoIdentityPasswordAuthentication>

@property (nonatomic, strong) NSString * usernameText;

@end
