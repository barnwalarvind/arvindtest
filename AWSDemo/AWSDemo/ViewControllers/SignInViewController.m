//
//  SignInViewController.m
//  AWSDemo
//
//  Created by Infoicon on 09/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import "SignInViewController.h"

@interface SignInViewController ()

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (nonatomic, strong) AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails*>* passwordAuthenticationCompletion;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}
- (void) viewWillAppear:(BOOL)animated {
    
    self.txtPassword.text = nil;
    self.txtEmail.text = self.usernameText;
}
- (IBAction)btnLoginAction:(id)sender {
    
     self.passwordAuthenticationCompletion.result = [[AWSCognitoIdentityPasswordAuthenticationDetails alloc] initWithUsername:self.txtEmail.text password:self.txtPassword.text];
}

#pragma mark
#pragma mark:- ==: AWSCognitoIdentityPasswordAuthentication Delegate :==

-(void) getPasswordAuthenticationDetails: (AWSCognitoIdentityPasswordAuthenticationInput *) authenticationInput  passwordAuthenticationCompletionSource: (AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails *> *) passwordAuthenticationCompletionSource {
    
    self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource;
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!self.usernameText)
            self.usernameText = authenticationInput.lastKnownUsername;
    });
    
}

-(void) didCompletePasswordAuthenticationStepWithError:(NSError*) error {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(error){
            [[[UIAlertView alloc] initWithTitle:error.userInfo[@"__type"]
                                        message:error.userInfo[@"message"]
                                       delegate:nil
                              cancelButtonTitle:nil
                              otherButtonTitles:@"Retry", nil] show];
        }else{
            self.usernameText = nil;
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    });
}

@end
