//
//  VerificationViewController.h
//  AWSDemo
//
//  Created by Infoicon on 09/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWSCognitoIdentityProvider.h"

@interface VerificationViewController : UIViewController

// From Previous Controller
@property (nonatomic, strong) AWSCognitoIdentityUser * user;
@property (nonatomic, strong) NSString * sentTo;


// Current Controller
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *lblCodeSentAt;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;

@end
