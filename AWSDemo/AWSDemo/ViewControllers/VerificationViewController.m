//
//  VerificationViewController.m
//  AWSDemo
//
//  Created by Infoicon on 09/12/16.
//  Copyright © 2016 InfoiconTechnologies. All rights reserved.
//

#import "VerificationViewController.h"
#import "SignInViewController.h"


@interface VerificationViewController ()


@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblUsername.text = self.user.username;
    self.lblCodeSentAt.text = [NSString stringWithFormat:@"Code sent to: %@", self.sentTo];
}

- (IBAction)btnConfirmAction:(id)sender {
    
    [[self.user confirmSignUp:self.txtCode.text forceAliasCreation:YES] continueWithBlock: ^id _Nullable(AWSTask<AWSCognitoIdentityUserConfirmSignUpResponse *> * _Nonnull task) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(task.error){
                if(task.error){
                    [[[UIAlertView alloc] initWithTitle:task.error.userInfo[@"__type"]
                                                message:task.error.userInfo[@"message"]
                                               delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil] show];
                }
            }else {
                //return to signin screen
                ((SignInViewController *)self.navigationController.viewControllers[0]).usernameText = self.user.username;
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        });
        return nil;
    }];

}

- (IBAction)btnResendAction:(id)sender {
    
    //resend the confirmation code
    [[self.user resendConfirmationCode] continueWithBlock:^id _Nullable(AWSTask<AWSCognitoIdentityUserResendConfirmationCodeResponse *> * _Nonnull task) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(task.error){
                [[[UIAlertView alloc] initWithTitle:task.error.userInfo[@"__type"]
                                            message:task.error.userInfo[@"message"]
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil] show];
            }else {
                [[[UIAlertView alloc] initWithTitle:@"Code Resent"
                                            message:[NSString stringWithFormat:@"Code resent to: %@", task.result.codeDeliveryDetails.destination]
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            }
        });
        return nil;
    }];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
